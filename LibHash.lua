-----------------------------------------------------------------------------------------------
-- Client Lua Script for LibHash
-- Copyright (c) NCsoft. All rights reserved
-----------------------------------------------------------------------------------------------
 
require "Window"
 
-----------------------------------------------------------------------------------------------
-- LibHash Module Definition
-----------------------------------------------------------------------------------------------
local LibHash  = {} 
 
-----------------------------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------------------------
local addon_version = "1.2"
local typeMd5 = 1
local typeMd5Sign = 2
local typeSha1 = 11
local typeSha1Sign = 12
local typeSha256 = 21
local typeSha256Sign = 22
 
-----------------------------------------------------------------------------------------------
-- Initialization
-----------------------------------------------------------------------------------------------
function LibHash:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self 

    -- initialize variables here
	self.typeSelected = typeMd5

    return o
end

function LibHash:Init()
    Apollo.RegisterAddon(self, "LibHash", "LibHash", {"LibMd5-1", "LibSha1-1", "LibSha256-1"})
end
 

-----------------------------------------------------------------------------------------------
-- LibHash OnLoad
-----------------------------------------------------------------------------------------------
function LibHash:OnLoad()
    -- load our form file
	self.xmlDoc = XmlDoc.CreateFromFile("LibHash.xml")
end

-----------------------------------------------------------------------------------------------
-- LibHash GetAsyncLoadStatus
-----------------------------------------------------------------------------------------------
function LibHash:GetAsyncLoadStatus()

	-- check for external dependencies here
	if g_AddonsLoaded == nil then
		g_AddonsLoaded = {}
	end

	-- Load a ref to our dependency libs
	self.libmd5 = Apollo.GetPackage("LibMd5-1").tPackage
	self.libsha1 = Apollo.GetPackage("LibSha1-1").tPackage
	self.libsha256 = Apollo.GetPackage("LibSha256-1").tPackage

	self.addon_version = addon_version
	
	if self.xmlDoc:IsLoaded() then
	    self.wndMain = Apollo.LoadForm(self.xmlDoc, "LibHashTestForm", nil, self)
		if self.wndMain == nil then
			Apollo.AddAddonErrorText(self, "Could not load the main window for some reason.")
			return Apollo.AddonLoadStatus.LoadingError
		end
		self.wndMain:Show(false, true)
		
		-- if the xmlDoc is no longer needed, you should set it to nil
		self.xmlDoc = nil
		
		-- Register handlers for events, slash commands and timer, etc.
		Apollo.RegisterSlashCommand("hash", "OnLibHashCommand", self)
		Apollo.RegisterSlashCommand("libhash", "OnLibHashCommand", self)

		-- Do additional Addon initialization here
		
		-- register our Addon so others can wait for it if they want
		g_AddonsLoaded["LibHash"] = true
		
		return Apollo.AddonLoadStatus.Loaded
	end
	return Apollo.AddonLoadStatus.Loading 
end

-----------------------------------------------------------------------------------------------
-- LibHash Functions
-----------------------------------------------------------------------------------------------
-- Define general functions here

-- on SlashCommand "/hash"
function LibHash:OnLibHashCommand()
	self.wndMain:Show(true) -- show the window
end

function LibHash:OnConfigure()
	self.wndMain:Show(true) -- show the window
end

---------------------------------------------------------------------------------------------------
-- LibHashTestForm Functions
---------------------------------------------------------------------------------------------------
-- when the OK button is clicked
function LibHash:OnHash()
	-- do the actual calculation
	input_text = self.wndMain:FindChild("InputEditBox"):GetText()
	input_key = self.wndMain:FindChild("KeyEditBox"):GetText()
	
	local output = ''
	if self.typeSelected == typeMd5 then
		output = self.libmd5:hash(input_text)
	elseif self.typeSelected == typeMd5Sign then
		output = self.libmd5:hmac(input_key, input_text)
	elseif self.typeSelected == typeSha1 then
		output = self.libsha1:hash(input_text)
	elseif self.typeSelected == typeSha1Sign then
		output = self.libsha1:hmac(input_key, input_text)
	elseif self.typeSelected == typeSha256 then
		output = self.libsha256:hash(input_text)
	elseif self.typeSelected == typeSha256Sign then
		output = self.libsha256:hmac(input_key, input_text)
	else
		Print("Unknown hashing algorithm selected. Please tell the author how you managed to do that.")
	end
	
	self.wndMain:FindChild("OutputEditBox"):SetText(output)
		
end

-- when the Cancel button is clicked
function LibHash:OnCancel()
	self.wndMain:Show(false) -- hide the window
end

function LibHash:ShowHideKeyControls(state)
	self.wndMain:FindChild("KeyLabel"):Show(state)
	self.wndMain:FindChild("KeyEditBox"):Show(state)
end

function LibHash:OnTypeSelectionMd5( wndHandler, wndControl, eMouseButton )
	self.typeSelected = typeMd5
	self:ShowHideKeyControls(false)
end

function LibHash:OnTypeSelectionMd5Sign( wndHandler, wndControl, eMouseButton )
	self.typeSelected = typeMd5Sign
	self:ShowHideKeyControls(true)
end

function LibHash:OnTypeSelectionSha1( wndHandler, wndControl, eMouseButton )
	self.typeSelected = typeSha1
	self:ShowHideKeyControls(false)
end

function LibHash:OnTypeSelectionSha1Sign( wndHandler, wndControl, eMouseButton )
	self.typeSelected = typeSha1Sign
	self:ShowHideKeyControls(true)
end

function LibHash:OnTypeSelectionSha256( wndHandler, wndControl, eMouseButton )
	self.typeSelected = typeSha256
	self:ShowHideKeyControls(false)
end

function LibHash:OnTypeSelectionSha256Sign( wndHandler, wndControl, eMouseButton )
	self.typeSelected = typeSha256Sign
	self:ShowHideKeyControls(true)
end



-----------------------------------------------------------------------------------------------
-- LibHash Instance
-----------------------------------------------------------------------------------------------
local LibHashInst = LibHash:new()
LibHashInst:Init()
